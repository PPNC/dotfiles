# ~/.bashrc
# If not running interactively, do nothing
case $- in
	*i*) ;;
	*) return;;
esac

# Resize window after each command acordingly
shopt -s checkwinsize

# Prompt
PS1='[\u@\h \w]\$ '

# Add .bin for user-made executables
if [ -d ~/.bin ] ; then
	PATH="$PATH:~/.bin"
fi

# Aliases
if [ -f ~/.bash_aliases ] ; then
	. ~/.bash_aliases
fi

# Completions
if [ -d /etc/bash_completion ] && ! shopt -oq posix; then
	. /etc/bash_completion
fi
if [ -f ~/.bash_completions ] ; then
	. ~/.bash_completions
fi

# History
export HISTSIZE=5000
export HISTCONTROL=ignoredups:ignorespace
export HISTIGNORE="clear":"cd ~":"cd ..":"cd -":"fg":"ls":"la"

# Vim as default editor
export EDITOR=vim
export VISUAL=vim

if [ -d ~/workspace/go ] ; then
	export GOPATH=~/workspace/go
	if [ -d ~/workspace/go/bin ] ; then
		export GOBIN=~/workspace/go/bin
		PATH=$PATH:$GOBIN
	fi
fi
