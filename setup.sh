#!/bin/bash
SCRIPTDIR=$(dirname $0)

# Bash config
rm -f $HOME/.bashrc \
	$HOME/.bash_aliases \
	$HOME/.bash_completions \
	$HOME/.bash_profile \
	$HOME/.inputrc
cp $SCRIPTDIR/bashrc $HOME/.bashrc
cp $SCRIPTDIR/bash_aliases $HOME/.bash_aliases
cp $SCRIPTDIR/bash_completions $HOME/.bash_completions
cp $SCRIPTDIR/bash_profile $HOME/.bash_profile
cp $SCRIPTDIR/inputrc $HOME/.inputrc
source $HOME/.bashrc

# Vim config
if [ -x "$(command -v vim)" ]; then
	echo 'Setting up vim'
	rm -rf $HOME/.vim \
		$HOME/.vimrc
	cp -r $SCRIPTDIR/vim $HOME/.vim
	cp $SCRIPTDIR/vimrc $HOME/.vimrc
	vim +PlugInstall +qall
fi

# i3 config
if [ -x "$(command -v i3-msg)" ]; then
	echo 'Setting up i3'
	rm -rf $HOME/.i3
	cp -r $SCRIPTDIR/i3 $HOME/.i3
	if [ -x "$(command -v i3status)" ]; then
		echo 'Setting up i3status'
		rm -f $HOME/.i3status.conf
		cp $SCRIPTDIR/i3status.conf $HOME/.i3status.conf
	fi
	i3-msg reload > /dev/null
fi

# Termite config
if [ -x "$(command -v termite)" ]; then
	echo 'Setting up termite'
	if [ -d $HOME/.config/termite ]; then
		rm -rf $HOME/.config/termite
	fi
	mkdir -p $HOME/.config/termite
	cp -r $SCRIPTDIR/termite/config $HOME/.config/termite/config
fi

# Git config
if [ -x "$(command -v git)" ]; then
	echo 'Setting up git'
	rm $HOME/.gitignore_global
	cp $SCRIPTDIR/gitignore_global $HOME/.gitignore_global
fi

echo 'Setup finished'
