# ~/.bash_aliases
alias l='ls --color=auto -lah'
# Default mkdir options
alias mkdir='mkdir -pv'
# Safe file management
alias mv='mv -vi'
alias cp='cp -vi'
alias rm='rm -v'
