"++ PLUGINS+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
call plug#begin('~/.vim/plugged')
Plug 'tpope/vim-surround'
Plug 'SirVer/ultisnips'
Plug 'ericbn/vim-solarized'
Plug 'scrooloose/nerdtree'
call plug#end()

"++ COLOUR SCHEME+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
set background=light
set termguicolors
silent! colorscheme solarized
syntax enable

"++ BASE OPTIONS++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
set backspace=2
set encoding=utf-8
set list
set listchars=tab:▶.
set number
set numberwidth=4
set relativenumber

"++ SNIPPETS++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
let g:UltiSnipsSnippetDirectories=[$HOME."/.vim/my-snippets"]
let g:UltiSnipsExpandTrigger="<tab>"
let g:UltiSnipsJumpForwardTrigger="<c-b>"
let g:UltiSnipsJumpBackwardTrigger="<c-z>"

"++ IDENTATION++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
set noexpandtab
set shiftwidth=4
set softtabstop=4
set tabstop=4

"++ MAPPINGS++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
" Move arround panels with Ctrl+HJKL
noremap <C-H> <C-W><Left>
noremap <C-J> <C-W><Up>
noremap <C-K> <C-W><Down>
noremap <C-L> <C-W><Right>
" Toggles
noremap <F2> :set invrelativenumber<CR>
noremap <F3> :set invnumber<CR>
" NERDTree
map <C-b> :NERDTreeToggle<CR>
